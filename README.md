Feing 
	- declarative Rest client 
	-higher abstract layer of RestTemplate (ordinary rest calls to remote services)


When using RestTemplate ( from springframework.web.client ):


Map<String,String> uriVariables = new HashMap<>();
uriVariables.put("productType",product.getProductType());
//params : param1 url , param2 responsetype; param3 pass the value to the pathvariabale 'productType'
RestTemplate<BigDecimal> responseEntity = new RestTemplate().getForEntity("http://localhost:8080/calculate-discount/{productType}", BigDecimal.class,uriVariables);


BigDecimal response = responseEntity.getBody();
bla bla...




Ribbon 
	- client side load balancing
	- use the feign configuration we've already done
	- distribute calls between different INSTANCES of the remote service (discount-service)
	- need to connect to naming server (Eureka)