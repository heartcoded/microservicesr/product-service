package com.dianabb.microservices.productservice.service;

import java.util.List;

import com.dianabb.microservices.productservice.model.Product;

public interface ProductService {

	public List<Product> getAllProducts();
	public Product getProductById(Long id);
	
}
