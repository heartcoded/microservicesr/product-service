package com.dianabb.microservices.productservice.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dianabb.microservices.productservice.DiscountServiceProxy;
import com.dianabb.microservices.productservice.exception.ProductNotFoundException;
import com.dianabb.microservices.productservice.model.Product;
import com.dianabb.microservices.productservice.repository.ProductRepository;
import com.dianabb.microservices.productservice.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private DiscountServiceProxy proxy;
	
	@Override
	public List<Product> getAllProducts() {
		List<Product> productsList = productRepository.findAll();
		if (productsList == null || productsList.isEmpty()) {
			throw new ProductNotFoundException("No product found");
		}

		return productRepository.findAll();
	}

	@Override
	public Product getProductById(Long id) {
		
		Optional<Product> optionalProduct = productRepository.findById(id);
		
		if (!optionalProduct.isPresent()) {
			throw new ProductNotFoundException("Product with id " + id + " not found.");
		}
		Product product = optionalProduct.get();
		//calls discount-service's method for calculating discount: discountPrice = price*(1-discount)
		BigDecimal discountResponse = proxy.retrieveDiscount(product.getProductType().toString());
		product.setDiscountPrice(product.getPrice().multiply(BigDecimal.ONE.subtract(discountResponse)));
		return product;
	}

}
