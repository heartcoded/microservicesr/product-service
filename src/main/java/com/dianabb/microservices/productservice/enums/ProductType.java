package com.dianabb.microservices.productservice.enums;

public enum ProductType {

	SHIRT, SKIRT, HAT, DRESS;

}
