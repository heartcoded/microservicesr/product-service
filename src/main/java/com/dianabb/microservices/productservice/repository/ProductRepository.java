package com.dianabb.microservices.productservice.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;

import com.dianabb.microservices.productservice.model.Product;
import com.dianabb.microservices.productservice.service.ProductService;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
