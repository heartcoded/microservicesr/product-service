package com.dianabb.microservices.productservice.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dianabb.microservices.productservice.model.Product;
import com.dianabb.microservices.productservice.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description = "This controller exposes a list of existing products. Also when you check each product individually you cand find the new price with discount based on discount-service logic ")
public class ProductController {

	final static Logger LOGGER = Logger.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	@GetMapping("/products")
	@ApiOperation(value = "Retrieve all products")
	public List<Product> getProducts() {
		return productService.getAllProducts();
	}

	@GetMapping("/products/{id}")
	@ApiOperation(value = "Retrieve product by id + also call discount-service method to calculate discount")
	public Product getProductById(@PathVariable("id") Long id){

		return productService.getProductById(id);
	}
	
}

/*
 * TODO: Ce ar insemna sa facem cate un call in backend pt fiecare element din
 * for ? care e solutia cea mai optima
 */
