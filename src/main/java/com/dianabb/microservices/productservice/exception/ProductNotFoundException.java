package com.dianabb.microservices.productservice.exception;

public class ProductNotFoundException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2024590852367425020L;

	public ProductNotFoundException(String message) {
		super(message);
	}

}
