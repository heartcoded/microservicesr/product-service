package com.dianabb.microservices.productservice;

import java.math.BigDecimal;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "discount-service", url = "localhost:8080")
@FeignClient(name = "discount-service")
@RibbonClient(name = "discount-service")
public interface DiscountServiceProxy {

	/*
	 * one of feign's bug : you must specify the path variable name even though is
	 * the same as method param.!!!!!!!!
	 */
	@GetMapping("/calculate-discount/{productType}")
	public BigDecimal retrieveDiscount(@PathVariable("productType") String productType);
}
