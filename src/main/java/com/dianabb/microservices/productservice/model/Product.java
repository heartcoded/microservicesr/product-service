package com.dianabb.microservices.productservice.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dianabb.microservices.productservice.enums.ProductType;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "products")
public class Product {

	@Id
	@GeneratedValue
	@ApiModelProperty(notes = "Product's id", required = true)
	private Long id;
	
	@ApiModelProperty(notes = "Product's name", required = true)
	private String name;
	
	@Column(name = "product_type")
	@Enumerated(EnumType.STRING) //declare that its value should be converted from what is effectively a String in the database to the ProductType type.
	@ApiModelProperty(notes = "Product's type {SKIRT, SHIRT, DRESS, HAT}", required = true)
	private ProductType productType;
	
	@ApiModelProperty(notes = "Product's price without discount", required = true)
	private BigDecimal price;
	
	@Column(name = "discount_price")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(notes = "Product's price including discount. In case of null, the property is not serialized", required = false)
	private BigDecimal discountPrice;

	public Product() {

	}

	public Product(Long id, String name, ProductType productType, BigDecimal price, BigDecimal discountPrice) {
		super();
		this.id = id;
		this.name = name;
		this.productType = productType;
		this.price = price;
		this.discountPrice = discountPrice;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", productType=" + productType + ", price=" + price
				+ ", discountPrice=" + discountPrice + "]";
	}

}
